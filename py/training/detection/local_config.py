'''
Stuff that will change per-machine.
'''
hmdhandrects_location = "/media/moses/traindata-jakob/HMDHandRects/"
egohands_convert = "/excluded_epics_from_sync/initial_frontend_T32624/hand-convert/ego-hands/"


kitchens_images = "/media/moses/traindata-jakob/EPIC-KITCHENS"
kitchens_annotations = "/media/moses/traindata-jakob/kitchen_labels/"
kitchens_only_1st_sequence = True # Set this to false if you're training a production model!

batch_size = 16